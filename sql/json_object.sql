-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION json_object" to load this file. \quit

CREATE FUNCTION json_object(array_vals text[])
RETURNS json
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT IMMUTABLE;

